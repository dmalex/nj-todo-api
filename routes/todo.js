const express = require('express')
const router = express.Router()

const todo = require('../controllers/todo')


// Retrieve all Todos
router.get("/get-items", todo.getTodos)

// Retrieve a single Todo by id
router.get("/get-item-by-id", todo.getTodoByID)

// Create a new Todo
router.post("/post-item-by-name", todo.postTodoByName)

// Update a Todo by id
router.put("/put-item-name-by-id", todo.putTodoNameByID)

// Delete a Todo by id
router.delete("/delete-item-by-id/:id", todo.deleteTodoByID)

// Delete all Todos
router.delete("/delete-items", todo.deleteTodos)

// Update partially a Todo by id
router.patch("/patch-item-name-by-id/:id", todo.patchTodoNameByID)


module.exports = router
