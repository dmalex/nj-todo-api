const express = require('express')
const app = express()
var cors = require('cors')
const routes = require('./routes/todo')

/*
// Set up a whitelist and check against it:
var whitelist = ['http://localhost:19006', 'http://localhost:8080']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}
// Then pass them to cors:
app.use(cors(corsOptions))
*/
app.use(cors()) // разрешить полный доступ к api

app.use(express.urlencoded({extended: true}))
const morgan = require('morgan')

const dotenv = require('dotenv')
dotenv.config()
const port = process.env.PORT || 8080

app.use(morgan('dev'))


const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/', routes)

app.use(function (req, res, next) {
    /*
    var origins = [
        'http://localhost:8080',
        'http://localhost:19006'
    ]

    for(var i = 0; i < origins.length; i++){
        var origin = origins[i]

        if(req.headers.origin.indexOf(origin) > -1){
            res.header('Access-Control-Allow-Origin', req.headers.origin)
            
        }
    }
    */

    res.header('Access-Control-Allow-Origin', '*')
    res.header("Access-Control-Allow-Methods",
               "GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS")
    res.header("Access-Control-Allow-Headers",
               "Origin, X-Requested-With, Content-Type, Accept, Authorization")
    next()
})


var mysql2 = require('mysql2')
const connection = mysql2.createConnection({
    host: process.env.HOST,
    user: process.env.USER_NAME,
    database: process.env.DATABASE,
    password: process.env.PASSWORD
})

connection.connect(function(error) {
    if(error) {
        return console.error('DB connection error: ' + error.message + '.')
    } else {
        console.log('Connection to MySQL server was succesfully estableshed.')
    }
})

app.listen(port, () => {
    console.log(`Todo API is listening on port: ${port}.`)
})


exports.connection = connection
