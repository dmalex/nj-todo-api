/// todo.js
/// todo model
/// Dmitry Alexandrov

const app = require('../app')

const Todo = function(todo) {
    // this.id = todo.id
    this.name = todo.name
}


Todo.postTodoByName = (newTodo, result) => {
  const sql = app.connection
  sql.query("INSERT INTO todo SET ?", newTodo, (err, res) => {
    if (err) {
      console.log("Error: ", err)
      result(err, null)
      return
    }
    console.log("Created todo: ", { id: res.insertId, ...newTodo })
    result(null, { id: res.insertId, ...newTodo })
  })
}


Todo.putTodoNameByID = (id, todo, result) => {
  const sql = app.connection
  sql.query(
    "UPDATE todo SET name = ? WHERE id = ?",
    [todo.name, id],
    (err, res) => {
      if (err) {
        console.log("Error: ", err)
        result(null, err)
        return
      }
      if (res.affectedRows == 0) {
        // Not found Todo with the id
        result({ kind: "not_found" }, null)
        return
      }
      console.log("Updated todo: ", { id: id, ...todo })
      result(null, { id: id, ...todo })
  })
}


Todo.getTodos = (result) => {
    const sql = app.connection
    sql.query("select * from todo", (err, res) => {
        if (err) {
            console.log("Error: ", err)
            result(err, null)
            return
        } else if (res.length) {
            console.log("Found todos:", res)
        }
        result(null, res)
    })
}


Todo.getTodoByID = (id, result) => {
  const sql = app.connection
  //console.log(`select * from todo where id = ${id}`)
  sql.query(`select * from todo where id = ${id}`, (err, res) => {
      if (err) {
          console.log("Error: ", err)
          result(err, null)
          return
      } else if (res.length) {
          console.log("Found todo:", res)
          result(null, res[0])
          return
      }
      result( { kind: "not_found" } , null)
  })
}


Todo.deleteTodoByID = (id, result) => {
    const sql = app.connection
    sql.query("DELETE FROM todo WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("Error: ", err)
        result(null, err)
        return
      }
      if (res.affectedRows == 0) {
        // Not found todo with id
        result({ kind: "not_found" }, null)
        return
      }
      console.log("Deleted todo with id: ", id, ".")
      result(null, res)
    })
}


Todo.deleteTodos = result => {
    const sql = app.connection
    sql.query("DELETE FROM todo", (err, res) => {
      if (err) {
        console.log("Error: ", err)
        result(null, err)
        return
      }
      console.log(`Deleted ${res.affectedRows} todos.`)
      result(null, res)
    })
}


Todo.patchTodoNameByID = (id, todo, result) => {
  const sql = app.connection
  sql.query(
    "UPDATE todo SET name = ? WHERE id = ?",
    [todo.name, id],
    (err, res) => {
      if (err) {
        console.log("Error: ", err)
        result(null, err)
        return
      }
      if (res.affectedRows == 0) {
        // Not found Todo with the id
        result({ kind: "not_found" }, null)
        return
      }
      console.log("Updated partially todo: ", { id: id, ...todo })
      result(null, { id: id, ...todo })
  })
}


module.exports = Todo
